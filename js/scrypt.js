var scene, camera, renderer, screen;
var geometry, material, cube;
var raycaster, controls, mouse
var crossObj, selectedObj;

init();
render();
window.addEventListener( 'mousemove', onMouseMove, false );
window.addEventListener( 'click', onMouseClick, false );
document.getElementById('close').addEventListener( 'click', close, false );

function init() {
    //  Сцена
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xf0f0ff );
    
    //  Свет
    light = new THREE.DirectionalLight( 0xffffff, 1 );
    light.position.set( 10, 10, 10 );
    scene.add( light );
    light1 = new THREE.DirectionalLight( 0xffffff, 1 );
    light1.position.set( -10, -10, -10 );
    scene.add( light1 );

    //  Объект
    left = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x990000, name: "left"}) );
    left.position.set(0, 0, 2);

    up = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x009900, name: "up"}) );
    up.position.set(0, 2, 0);
    up.rotateX( -1.57 );
    
    right = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x000099, name: "right"}) );
    right.position.set(0, 0, -2);
    right.rotateX( 3.14 );

    down = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x660000, name: "down"}) );
    down.position.set(0, -2, 0);
    down.rotateX( 1.57 );

    front = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x000033, name: "front"}) );
    front.position.set(2, 0, 0);
    front.rotateY( 1.57 );                

    back = new THREE.Mesh( new THREE.PlaneGeometry(4, 4), new THREE.MeshLambertMaterial({ color: 0x003300, name: "back"}) )
    back.position.set(-2, 0, 0);
    back.rotateY( -1.57 );  

    scene.add( front, up, down, left, right, back );

    //  Камера
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 50 );
    camera.position.set(10, 10, 10);

    //  Рендер
    renderer = new THREE.WebGLRenderer({antialias:true});
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    controls = new THREE.OrbitControls( camera, renderer.domElement );
                
    raycaster = new THREE.Raycaster();
    mouse = new THREE.Vector2();
}

function onMouseMove( event ) {
    event.preventDefault();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

function render() {
    requestAnimationFrame( render );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    if ( intersects.length > 0 ) {
        if ( crossObj != intersects[0].object ) {
            if ( crossObj ) crossObj.material.color.setHex( crossObj.currentHex );
            crossObj = intersects[0].object;
            crossObj.currentHex = crossObj.material.color.getHex();
            crossObj.material.color.setHex( 0xff0000 );
            selectedObj = crossObj.material.name;
        }
    } else {
        if ( crossObj ) crossObj.material.color.setHex( crossObj.currentHex );
        crossObj = null;
        selectedObj = null;
    }
    renderer.render( scene, camera );
}

function onMouseClick() {
    if ( selectedObj ) {
        document.getElementById('msg').style = 'display: block';
        document.getElementById('titleMsg').textContent = selectedObj;
    }
}

function close() {
    document.getElementById('msg').style = '';
}